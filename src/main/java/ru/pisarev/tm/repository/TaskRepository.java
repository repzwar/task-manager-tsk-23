package ru.pisarev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.repository.ITaskRepository;
import ru.pisarev.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Nullable
    @Override
    public List<Task> findAllTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null) return null;
        return entities.values().stream()
                .filter(o -> userId.equals(o.getUserId()) && projectId.equals(o.getProjectId()))
                .collect(Collectors.toList());
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null) return;
        findAllTaskByProjectId(userId, projectId).forEach(o -> entities.remove(o.getId()));
    }

    @Nullable
    @Override
    public Task bindTaskToProjectById(@NotNull final String userId, @Nullable final String taskId, @Nullable final String projectId) {
        if (projectId == null || taskId == null) return null;
        @Nullable final Task task = findById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Nullable
    @Override
    public Task unbindTaskById(@NotNull final String userId, @Nullable final String id) {
        if (id == null) return null;
        @Nullable final Task task = findById(userId, id);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    @Nullable
    @Override
    public Task findByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null) return null;
        return entities.values().stream()
                .filter(o -> userId.equals(o.getUserId()) && name.equals(o.getName()))
                .limit(1)
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Task findByIndex(@NotNull final String userId, final int index) {
        @NotNull List<Task> entities = findAll(userId);
        return entities.get(index);
    }

    @Nullable
    @Override
    public Task removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null) return null;
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        entities.remove(task.getId());
        return task;
    }

    @Nullable
    @Override
    public Task removeByIndex(@NotNull final String userId, final int index) {
        @Nullable final Task task = findByIndex(userId, index);
        if (task == null) return null;
        entities.remove(task.getId());
        return task;
    }

}
