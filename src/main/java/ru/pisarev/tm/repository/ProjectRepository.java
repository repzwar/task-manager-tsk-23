package ru.pisarev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.repository.IProjectRepository;
import ru.pisarev.tm.model.Project;

import java.util.List;

public final class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

    @Nullable
    @Override
    public Project findByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null) return null;
        return entities.values().stream()
                .filter(o -> userId.equals(o.getUserId()) && name.equals(o.getName()))
                .limit(1)
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Project findByIndex(@NotNull final String userId, final int index) {
        @NotNull List<Project> entities = findAll(userId);
        return entities.get(index);
    }

    @Nullable
    @Override
    public Project removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null) return null;
        @Nullable final Project project = findByName(userId, name);
        if (project == null) return null;
        entities.remove(project.getId());
        return project;
    }

    @Nullable
    @Override
    public Project removeByIndex(@NotNull final String userId, final int index) {
        @Nullable final Project project = findByIndex(userId, index);
        if (project == null) return null;
        entities.remove(project.getId());
        return project;
    }

}
