package ru.pisarev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.model.AbstractBusinessEntity;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> {

    @NotNull
    protected final Map<String, E> entities = new LinkedHashMap<>();

    @NotNull
    public List<E> findAll(@NotNull final String userId) {
        return entities.values().stream()
                .filter(o -> userId.equals(o.getUserId()))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @NotNull
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return entities.values().stream()
                .filter(o -> userId.equals(o.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public void addAll(@NotNull final String userId, @Nullable Collection<E> collection) {
        if (collection == null) return;
        collection.forEach(o -> o.setUserId(userId));
        Map<String, E> newEntities = collection.stream()
                .collect(Collectors.toMap(E::getId, Function.identity(), (o1, o2) -> o1, LinkedHashMap::new));
        entities.putAll(newEntities);
    }

    @Nullable
    public E add(@NotNull final String userId, @Nullable final E entity) {
        if (entity == null) return null;
        entity.setUserId(userId);
        entities.put(entity.getId(), entity);
        return entity;
    }

    @Nullable
    public E findById(@NotNull final String userId, @Nullable final String id) {
        if (id == null) return null;
        @Nullable E entity = entities.get(id);
        if (entity == null) return null;
        if (userId.equals(entity.getId())) return entity;
        return null;
    }

    public void clear(@NotNull final String userId) {
        findAll(userId).forEach(o -> entities.remove(o.getId()));
    }

    @Nullable
    public E removeById(@NotNull final String userId, @Nullable final String id) {
        if (id == null) return null;
        @Nullable final E entity = findById(userId, id);
        if (entity == null) return null;
        entities.remove(entity.getId());
        return entity;
    }

    @Nullable
    public E remove(@NotNull final String userId, @NotNull final E entity) {
        if (!userId.equals(entity.getUserId())) return null;
        entities.remove(entity.getId());
        return entity;
    }

    public int getSize(@NotNull final String userId) {
        return findAll(userId).size();
    }

}
