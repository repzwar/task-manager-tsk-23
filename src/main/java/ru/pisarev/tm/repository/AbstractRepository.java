package ru.pisarev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IRepository;
import ru.pisarev.tm.model.AbstractEntity;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final Map<String, E> entities = new LinkedHashMap<>();

    @NotNull
    @Override
    public List<E> findAll() {
        return new ArrayList<>(entities.values());
    }

    @Override
    public void addAll(@Nullable final Collection<E> collection) {
        if (collection == null) return;
        @NotNull Map<String, E> newEntities = collection.stream()
                .collect(Collectors.toMap(E::getId, Function.identity(), (o1, o2) -> o1, LinkedHashMap::new));
        entities.putAll(newEntities);
    }

    @Nullable
    @Override
    public E add(@Nullable final E entity) {
        if (entity == null) return null;
        entities.put(entity.getId(), entity);
        return entity;
    }

    @Nullable
    @Override
    public E findById(@Nullable final String id) {
        if (id == null) return null;
        return entities.get(id);
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Nullable
    @Override
    public E removeById(@Nullable final String id) {
        if (id == null) return null;
        @Nullable final E entity = findById(id);
        if (entity == null) return null;
        entities.remove(id);
        return entity;
    }

    @Nullable
    @Override
    public E remove(final E entity) {
        if (entity == null) return null;
        entities.remove(entity.getId());
        return entity;
    }
}
