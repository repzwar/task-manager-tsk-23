package ru.pisarev.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.repository.ICommandRepository;
import ru.pisarev.tm.api.repository.IProjectRepository;
import ru.pisarev.tm.api.repository.ITaskRepository;
import ru.pisarev.tm.api.repository.IUserRepository;
import ru.pisarev.tm.api.service.*;
import ru.pisarev.tm.command.AbstractCommand;
import ru.pisarev.tm.command.auth.*;
import ru.pisarev.tm.command.project.*;
import ru.pisarev.tm.command.system.*;
import ru.pisarev.tm.command.task.*;
import ru.pisarev.tm.command.user.UserLockByLoginCommand;
import ru.pisarev.tm.command.user.UserRemoveByLoginCommand;
import ru.pisarev.tm.command.user.UserUnlockByLoginCommand;
import ru.pisarev.tm.constant.TerminalConst;
import ru.pisarev.tm.enumerated.Role;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.exception.system.UnknownCommandException;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.model.Task;
import ru.pisarev.tm.repository.CommandRepository;
import ru.pisarev.tm.repository.ProjectRepository;
import ru.pisarev.tm.repository.TaskRepository;
import ru.pisarev.tm.repository.UserRepository;
import ru.pisarev.tm.service.*;
import ru.pisarev.tm.util.TerminalUtil;

import static ru.pisarev.tm.util.TerminalUtil.displayWait;
import static ru.pisarev.tm.util.TerminalUtil.displayWelcome;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ILogService logService = new LogService();

    public void start(String... args) {
        displayWelcome();
        if (runArgs(args)) System.exit(0);
        process();
    }

    {
        registry(new DisplayCommand());
        registry(new AboutDisplayCommand());
        registry(new ArgumentsDisplayCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoDisplayCommand());
        registry(new VersionDisplayCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectListShowCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new TaskBindTaskToProjectByIdCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFindAllTaskByProjectIdCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskShowListCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUnbindTaskByIdCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new LoginCommand());
        registry(new LogoutCommand());
        registry(new PasswordChangeCommand());
        registry(new ProfileUpdateCommand());
        registry(new ProfileViewCommand());
        registry(new RegistryCommand());
        registry(new UserLockByLoginCommand());
        registry(new UserRemoveByLoginCommand());
        registry(new UserUnlockByLoginCommand());
    }

    {
        @NotNull String adminId = userService.add("admin", "admin", "admin@a").getId();
        userService.findByLogin("admin").setRole(Role.ADMIN);
        @NotNull String userId = userService.add("user", "user").getId();

        projectService.add(adminId, new Project("Project C", "-")).setStatus(Status.COMPLETED);
        projectService.add(adminId, new Project("Project A", "-"));
        projectService.add(adminId, new Project("Project B", "-")).setStatus(Status.IN_PROGRESS);
        projectService.add(adminId, new Project("Project D", "-")).setStatus(Status.COMPLETED);
        taskService.add(adminId, new Task("Task C", "-")).setStatus(Status.COMPLETED);
        taskService.add(adminId, new Task("Task A", "-"));
        taskService.add(adminId, new Task("Task B", "-")).setStatus(Status.IN_PROGRESS);
        taskService.add(adminId, new Task("Task D", "-")).setStatus(Status.COMPLETED);
        taskService.add(userId, new Task("Task B2", "-")).setStatus(Status.IN_PROGRESS);
    }

    private boolean runArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable AbstractCommand command = commandService.getCommandByArg(args[0]);
        if (command == null) throw new UnknownCommandException(args[0]);
        authService.checkRoles(command.roles());
        command.execute();
        return true;
    }

    private void runCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return;
        @Nullable AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException(command);
        authService.checkRoles(abstractCommand.roles());
        abstractCommand.execute();
    }

    private void registry(@Nullable AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void process() {
        logService.debug("Test environment.");
        @Nullable String command = "";
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            try {
                displayWait();
                command = TerminalUtil.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
