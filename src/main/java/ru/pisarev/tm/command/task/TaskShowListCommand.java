package ru.pisarev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.enumerated.Sort;
import ru.pisarev.tm.model.Task;
import ru.pisarev.tm.model.User;
import ru.pisarev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskShowListCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        @NotNull User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sort = TerminalUtil.nextLine();
        @Nullable List<Task> tasks;
        if (sort == null || sort.isEmpty()) tasks = serviceLocator.getTaskService().findAll(user.getId());
        else {
            @NotNull Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = serviceLocator.getTaskService().findAll(user.getId(), sortType.getComparator());
        }
        int index = 1;
        for (@NotNull Task project : tasks) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
    }
}
