package ru.pisarev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.exception.entity.TaskNotFoundException;
import ru.pisarev.tm.model.Task;
import ru.pisarev.tm.model.User;
import ru.pisarev.tm.util.TerminalUtil;

import static ru.pisarev.tm.util.TerminalUtil.incorrectValue;

public class TaskBindTaskToProjectByIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-bind-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Bind task to project.";
    }

    @Override
    public void execute() {
        @NotNull User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter task id");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final Task task = serviceLocator.getTaskService().findById(user.getId(), taskId);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter project id");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final Task taskUpdated = serviceLocator.getProjectTaskService().bindTaskById(user.getId(), taskId, projectId);
        if (taskUpdated == null) incorrectValue();
    }
}
