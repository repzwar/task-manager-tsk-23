package ru.pisarev.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.service.ServiceLocator;
import ru.pisarev.tm.enumerated.Role;

public abstract class AbstractCommand {

    @Nullable
    protected ServiceLocator serviceLocator;

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @Nullable
    public Role[] roles() {
        return null;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        @Nullable String name = name();
        @Nullable String arg = arg();
        @Nullable String description = description();

        if (name != null && !name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += " [" + arg + "] ";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

}
