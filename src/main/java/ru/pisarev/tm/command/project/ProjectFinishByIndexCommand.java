package ru.pisarev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.ProjectAbstractCommand;
import ru.pisarev.tm.exception.entity.ProjectNotFoundException;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.model.User;
import ru.pisarev.tm.util.TerminalUtil;

public class ProjectFinishByIndexCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-finish-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish project by index.";
    }

    @Override
    public void execute() {
        @NotNull User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Project project = serviceLocator.getProjectService().finishByIndex(user.getId(), index);
        if (project == null) throw new ProjectNotFoundException();
    }
}
