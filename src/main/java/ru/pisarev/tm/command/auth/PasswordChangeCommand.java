package ru.pisarev.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.AuthAbstractCommand;
import ru.pisarev.tm.model.User;
import ru.pisarev.tm.util.TerminalUtil;

public class PasswordChangeCommand extends AuthAbstractCommand {
    @Override
    public String name() {
        return "password-change";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change password.";
    }

    @Override
    public void execute() {
        @NotNull User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter new password");
        @Nullable final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(user.getId(), password);
    }
}
