package ru.pisarev.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.command.AuthAbstractCommand;
import ru.pisarev.tm.model.User;

public class ProfileViewCommand extends AuthAbstractCommand {
    @Override
    public String name() {
        return "view-profile";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "View profile.";
    }

    @Override
    public void execute() {
        @NotNull User user = serviceLocator.getAuthService().getUser();
        System.out.println("Login: " + user.getLogin());
        System.out.println("Email: " + user.getEmail());
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("Middle name: " + user.getMiddleName());
    }
}
