package ru.pisarev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IBusinessRepository;
import ru.pisarev.tm.api.IBusinessService;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.model.AbstractBusinessEntity;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    @NotNull
    private final IBusinessRepository<E> repository;

    @NotNull
    public AbstractBusinessService(@NotNull final IBusinessRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return repository.findAll(userId);
    }

    @Override
    public void addAll(@NotNull final String userId, @Nullable final Collection<E> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.addAll(userId, collection);
    }

    @Nullable
    @Override
    public E add(@NotNull final String userId, @Nullable final E entity) {
        if (entity == null) return null;
        return repository.add(userId, entity);
    }

    @NotNull
    @Override
    public E findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    public void clear(@NotNull final String userId) {
        repository.clear(userId);
    }

    @NotNull
    @Override
    public E removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull Optional<String> optionalId = Optional.ofNullable(id);
        return repository.removeById(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Nullable
    @Override
    public E remove(@NotNull final String userId, @Nullable final E entity) {
        if (entity == null) return null;
        return repository.remove(userId, entity);
    }

}
