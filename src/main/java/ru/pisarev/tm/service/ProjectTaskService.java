package ru.pisarev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.repository.IProjectRepository;
import ru.pisarev.tm.api.repository.ITaskRepository;
import ru.pisarev.tm.api.service.IProjectTaskService;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.empty.EmptyIndexException;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.exception.entity.ProjectNotFoundException;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.model.Task;

import java.util.List;
import java.util.Optional;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    public ProjectTaskService(@NotNull final ITaskRepository taskRepository, @NotNull final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @NotNull
    @Override
    public List<Task> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public Task bindTaskById(
            @NotNull final String userId, @Nullable final String taskId, @Nullable final String projectId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        return taskRepository.bindTaskToProjectById(userId, taskId, projectId);
    }

    @NotNull
    @Override
    public Task unbindTaskById(@NotNull final String userId, @Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        return taskRepository.unbindTaskById(userId, taskId);
    }

    @NotNull
    @Override
    public Project removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(userId, projectId);
    }

    @NotNull
    @Override
    public Project removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull String projectId = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new)
                .getId();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(userId, projectId);
    }

    @NotNull
    @Override
    public Project removeProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull String projectId = Optional.ofNullable(projectRepository.findByName(userId, name))
                .orElseThrow(ProjectNotFoundException::new)
                .getId();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(userId, projectId);
    }

}